const fakePromesse1 = (x) => new Promise((resolve) => {
  if (x > 3) return resolve(4 + x);
  return resolve(x);
});

const fakePromesse2 = (x) => new Promise((resolve) => {
  if (x % 2 === 0) return resolve(3 - x);
  return resolve(x + 1);
});

const fakePromesse3 = (x) => new Promise((resolve) => {
  if (x > 5) return resolve(x + 2);
  return resolve(x - 2);
});

module.exports = {
  fakePromesse1,
  fakePromesse2,
  fakePromesse3,
};
