const axios = require('axios');
const cheerio = require('cheerio');

/*
* Cette fonction retourne les titres de deux pages Wikipedia
* */
function getTitles() {
  return axios.get('https://fr.wikipedia.org/wiki/World_Wide_Web').then((page) => {
    const $ = cheerio.load(page.data);

    return axios.get('https://fr.wikipedia.org/wiki/Internet').then((page2) => {
      const $2 = cheerio.load(page2.data);

      return [$('h1').text(), $2('h1').text()];
    });
  });
}

const { fakePromesse1, fakePromesse2, fakePromesse3 } = require('./promesses');

/*
* Cette fonction doit envoyer «nombre» à la fakePromesse1.
* Au résultat de cette promesse, on doit ajouter 5. On passe cette valeur à fakePromesse2.
* Au résultat de cette 2e promesse, on soustrait 1. On passe cette valeur à fakePromesse3
* et la promesse finale doit retourner ce nombre.
* */
function meliMelo(nombre) {
  return fakePromesse1(nombre)
    .then((resultat1) => fakePromesse2(resultat1 + 5))
    .then((resultat2) => fakePromesse3(resultat2 + 1));
}

// Tests ici
const main = async () => {
  const result = await getTitles();
  console.log(result, ' doit égaler ', ['World Wide Web', 'Internet']);

  console.log(await meliMelo(1), 'doit égaler', -4);
  console.log(await meliMelo(4), 'doit égaler', 17);
  console.log(await meliMelo(0), 'doit égaler', 9);
};
main();
